import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { NgxsModule } from '@ngxs/store';
import { TodoState } from '../todo.state';
import { ToolbarComponent } from '../toolbar/toolbar.component';

describe('ToolbarComponent', () => {
    let component: ToolbarComponent;
    let fixture: ComponentFixture<ToolbarComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([TodoState])],
            declarations: [ToolbarComponent]
        })
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ToolbarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
