import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store, Select } from '@ngxs/store';
import { CreateTodo, ChangeText } from '../todo.actions';
import { TodoState } from '../todo.state';

@Component({
    selector: 'app-todo-form',
    templateUrl: './todo-form.component.html',
    styleUrls: ['./todo-form.component.css']
})
export class TodoFormComponent {

    todoForm: FormGroup;

    constructor(private fb: FormBuilder,
        private store: Store) {
        this.todoForm = fb.group({
            'descriptionCtrl': ''
        });
    }
    // @Select(TodoState.)

    submitTodo() {
        const { descriptionCtrl } = this.todoForm.value;
        this.store.dispatch(new CreateTodo(descriptionCtrl));
    }

    onChangeText($event) {
        this.store.dispatch(new ChangeText($event))
    }
}
