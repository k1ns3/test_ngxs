import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { TodoListComponent } from './todo-list.component';
import { NgxsModule } from '@ngxs/store';
import { TodoState } from '../todo.state';
import { ToolbarComponent } from '../toolbar/toolbar.component';

describe('TodoListComponent', () => {
    let component: TodoListComponent;
    let fixture: ComponentFixture<TodoListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([TodoState])],
            declarations: [TodoListComponent, ToolbarComponent]
        })
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TodoListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
